import React from "react";

import Routes from "./routes";
import "./assets/styles/style.scss";

const App: React.FC = () => <Routes />;
export default App;
