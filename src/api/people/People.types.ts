import { Person } from "../../types/person.types";

export type AddOrUpdatePeopleInputs = {
  data: Person;
};

export type GetAllPeopleInputs = {
  page: number;
};
