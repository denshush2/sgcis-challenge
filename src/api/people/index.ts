import { AxiosResponse } from "axios";

import { Person } from "../../types/person.types";
import API from "../../config/axios";
import { AddOrUpdatePeopleInputs, GetAllPeopleInputs } from "./People.types";

export default {
  getAllPeople: async ({
    page
  }: GetAllPeopleInputs): Promise<Person[] | null> => {
    try {
      const response: AxiosResponse<Person[]> = await API.request({
        url: "https://my-json-server.typicode.com/sgcis/codetest/persons",
        method: "GET",
        params: {
          _limit: 10,
          _page: page
        }
      });
      if (!response.data) {
        return null;
      }
      return response.data;
    } catch (e) {
      return null;
    }
  },
  updatePerson: async ({
    data
  }: AddOrUpdatePeopleInputs): Promise<Person | null> => {
    try {
      const response: AxiosResponse<Person> = await API.request({
        url: `https://my-json-server.typicode.com/sgcis/codetest/persons/${data.id}`,
        method: "PUT",
        data
      });
      if (!response.data) {
        return null;
      }
      return response.data;
    } catch (e) {
      return null;
    }
  },
  deletePerson: async ({
    data
  }: AddOrUpdatePeopleInputs): Promise<Person | null> => {
    try {
      const response: AxiosResponse<Person> = await API.request({
        url: `https://my-json-server.typicode.com/sgcis/codetest/persons/${data.id}`,
        method: "DELETE",
        data
      });
      if (!response.data) {
        return null;
      }
      return response.data;
    } catch (e) {
      return null;
    }
  },
  createPerson: async ({
    data
  }: AddOrUpdatePeopleInputs): Promise<Person | null> => {
    try {
      const response: AxiosResponse<Person> = await API.request({
        url: `https://my-json-server.typicode.com/sgcis/codetest/persons`,
        method: "POST",
        data
      });
      if (!response.data) {
        return null;
      }
      return response.data;
    } catch (e) {
      return null;
    }
  }
};
