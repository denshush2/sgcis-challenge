import React, { useEffect, useState } from "react";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import { People as PeopleAPI } from "../../api";
import { Person } from "../../types/person.types";
import Table from "../../containers/Table";
import Spinner from "../../components/spinner";
import PaginationComponent from "../../components/Pagination";
import { HandlePaginationInputs } from "./MainPage.types";
import "./style.scss";

const MainPage: React.FC = () => {
  const [people, setPeople] = useState<Person[]>([]);
  const [isLoading, setIsLoading] = useState<Boolean>(false);
  const [activePage, setActivePage] = useState<number>(1);
  useEffect(() => {
    getPeople();
  }, []);

  const getPeople = async () => {
    setIsLoading(true);
    const response = await PeopleAPI.getAllPeople({ page: 1 });
    setIsLoading(false);
    if (response) setPeople(response);
  };
  const handlePagination: Function = async ({
    pageNumber
  }: HandlePaginationInputs) => {
    setIsLoading(true);
    setActivePage(pageNumber);
    const response = await PeopleAPI.getAllPeople({ page: pageNumber });
    if (response) setPeople(response);
    setIsLoading(false);
  };

  return (
    <div className="main-page">
      <h1 className="title">SGCIS CHALLENGE</h1>
      {isLoading ? (
        <Spinner />
      ) : (
        <>
          <Table people={people} />{" "}
          <PaginationComponent
            active={activePage}
            onChange={handlePagination}
          />
        </>
      )}
      <ToastContainer />
    </div>
  );
};

export default MainPage;
