export type HandlePaginationInputs = {
  pageNumber: number;
};
