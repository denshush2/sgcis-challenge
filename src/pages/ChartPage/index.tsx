import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { Chart } from "react-google-charts";

import { Person } from "../../types/person.types";
import Spinner from "../../components/spinner";
import "./style.scss";
import { Button } from "react-bootstrap";

const ChartComponent: React.FC = () => {
  const history = useHistory();
  const [person, setPerson] = useState<Person | null>(null);
  useEffect(() => {
    if (history) {
      if (history.location.state && (history.location.state as any).person) {
        setPerson((history.location.state as any).person);
      }
    }
  }, [history]);

  return (
    <div className="chart-page">
      <Chart
        width={"100%"}
        height={"100%"}
        chartType="Bar"
        loader={
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              flexDirection: "column",
              alignItems: "center"
            }}
          >
            <h1>SGCIS CHALLENGE</h1>
            <Spinner />
          </div>
        }
        data={[
          ["Year", "Sales", "Expenses", "Profit"],
          [
            "2014",
            Math.floor(Math.random() * 1200),
            Math.floor(Math.random() * 1200),
            Math.floor(Math.random() * 1200)
          ],
          [
            "2015",
            Math.floor(Math.random() * 1200),
            Math.floor(Math.random() * 1200),
            Math.floor(Math.random() * 1200)
          ],
          [
            "2016",
            Math.floor(Math.random() * 1200),
            Math.floor(Math.random() * 1200),
            Math.floor(Math.random() * 1200)
          ],
          [
            "2017",
            Math.floor(Math.random() * 1200),
            Math.floor(Math.random() * 1200),
            Math.floor(Math.random() * 1200)
          ]
        ]}
        options={{
          // Material design options
          chart: {
            title: ` Chart for ${person?.name}`,
            subtitle: "Sales, Expenses, and Profit: 2014-2017"
          }
        }}
        // For tests
        rootProps={{ "data-testid": "2" }}
      />
      <Button className="go-back-button" onClick={() => history.push("/")}>
        Go Back
      </Button>
    </div>
  );
};

export default ChartComponent;
