export type PaginationInputs = {
  active: number;
  onChange: Function;
};
