import React, { useEffect, useState } from "react";
import { Pagination } from "react-bootstrap";

import { PaginationInputs } from "./Pagination.types";

const PaginationComponent: React.FC<PaginationInputs> = ({
  active,
  onChange
}) => (
  <Pagination>
    {[1, 2, 3].map(item => (
      <Pagination.Item
        onClick={() => onChange({ pageNumber: item })}
        key={item}
        active={item === active}
      >
        {item}
      </Pagination.Item>
    ))}
  </Pagination>
);

export default PaginationComponent;
