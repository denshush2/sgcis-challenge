import { Person } from "../../types/person.types";
import { Mode } from "../../types/modal.types";

export type Props = {
  isOpen: boolean;
  person: Person | null;
  onHideModal: VoidFunction;
  mode: Mode;
  onSubmit: Function;
};

export type Titles = {
  edit: string;
  show: string;
  create: string;
};

export type PersonType = {
  name: string;
  value: number;
};

export type FormLabelType = {
  name: string;
  age: string;
  id: string;
  type: string;
};

export type SubmitButtons = {
  save: string;
  create: string;
};

export type FormType = {
  name: string;
  age: number;
  id: number;
  student: number;
};

export type HandleChangeInputs = {
  name: string;
  value: string;
};
