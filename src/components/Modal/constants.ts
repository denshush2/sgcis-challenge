import {
  Titles,
  PersonType,
  FormLabelType,
  SubmitButtons
} from "./Modal.types";

export const titles: Titles = {
  edit: "Edit person",
  show: "Show person",
  create: "Add new person"
};
export const submitButtonsLabels: SubmitButtons = {
  save: "Save changes",
  create: "Create Person"
};

export const formLabels: FormLabelType = {
  age: "Age",
  id: "ID",
  name: "Name",
  type: "Type"
};

export const personTypes: PersonType[] = [
  {
    name: "Student",
    value: 2
  },
  {
    name: "Teacher",
    value: 1
  }
];
