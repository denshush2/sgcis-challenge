import React, { useState, useEffect } from "react";
import { Modal, Form, Col, Row, Button } from "react-bootstrap";

import { Props, HandleChangeInputs } from "./Modal.types";
import { Person } from "../../types/person.types";
import {
  titles,
  formLabels,
  submitButtonsLabels,
  personTypes
} from "./constants";

const ModalComponent: React.FC<Props> = ({
  isOpen,
  onHideModal,
  person,
  mode,
  onSubmit
}) => {
  const [form, setForm] = useState<Person>({
    name: "",
    age: 0,
    id: 0,
    personTypeId: 0
  });

  useEffect(() => {
    if (person) {
      setForm({ ...person });
    }
    return () => {
      console.log("unmound");
      setForm({ name: "", age: 0, id: 0, personTypeId: 0 });
    };
  }, [person]);

  const handleChange = ({ name, value }: HandleChangeInputs) => {
    if (name === "id" || name === "age" || name === "personTypeId") {
      setForm(prevState => ({ ...prevState, [name]: parseInt(value) }));
    } else {
      setForm(prevState => ({ ...prevState, [name]: value }));
    }
  };

  return (
    <Modal show={isOpen} onHide={onHideModal}>
      <Modal.Header closeButton>
        <Modal.Title>
          <span>{titles[mode]}: </span>
          <i>{form.name}</i>
        </Modal.Title>
      </Modal.Header>
      <Form
        onSubmit={(event: React.ChangeEvent<HTMLFormElement>) => {
          event.preventDefault();
          onSubmit({ mode, data: form });
        }}
      >
        <Modal.Body>
          <Form.Group as={Row}>
            <Form.Label column sm="2">
              <b>{formLabels.id}</b>
            </Form.Label>
            <Col sm="10">
              <Form.Control
                plaintext={mode === "show"}
                readOnly={mode === "show"}
                type="number"
                onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                  handleChange({ name: "id", value: event.target.value })
                }
                value={form.id.toString()}
              />
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Form.Label column sm="2">
              <b>{formLabels.name}</b>
            </Form.Label>
            <Col sm="10">
              <Form.Control
                plaintext={mode === "show"}
                readOnly={mode === "show"}
                value={form.name}
                onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                  handleChange({ name: "name", value: event.target.value })
                }
              />
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Form.Label column sm="2">
              <b>{formLabels.age}</b>
            </Form.Label>
            <Col sm="10">
              <Form.Control
                type="number"
                plaintext={mode === "show"}
                readOnly={mode === "show"}
                value={form.age.toString()}
                onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                  handleChange({ name: "age", value: event.target.value })
                }
              />
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Form.Label column sm="2">
              <b>{formLabels.type}</b>
            </Form.Label>
            <Col sm="10">
              <Form.Control
                plaintext={mode === "show"}
                readOnly={mode === "show"}
                disabled={mode === "show"}
                as="select"
                // as={mode === "show" ? "input" : "select"}
                onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                  handleChange({
                    name: "personTypeId",
                    value: event.target.value
                  })
                }
                value={form.personTypeId.toString()}
              >
                {personTypes.map(type => (
                  <option key={type.value} value={type.value}>
                    {type.name}
                  </option>
                ))}
              </Form.Control>
            </Col>
          </Form.Group>
        </Modal.Body>
        {mode !== "show" && (
          <Modal.Footer>
            <Button variant="primary" type="submit">
              {mode === "create"
                ? submitButtonsLabels.create
                : submitButtonsLabels.save}
            </Button>
          </Modal.Footer>
        )}
      </Form>
    </Modal>
  );
};

export default ModalComponent;
