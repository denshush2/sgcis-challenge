import React from "react";
import { Spinner } from "react-bootstrap";

const SpinnerComponent: React.FC = () => <Spinner animation="border" />;

export default SpinnerComponent;
