import React, { useState } from "react";
import { Table, ButtonGroup, Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";

import Modal from "../../components/Modal";
import { TableProps, OpenModalInputs, SubmitFormInputs } from "./Table.types";
import { Person } from "../../types/person.types";
import { columns, actionsButtons } from "./constants";
import { Mode } from "../../types/modal.types";
import { People as PeopleAPI } from "../../api";
import "./style.scss";

const TableComponent: React.FC<TableProps> = ({ people }) => {
  const history = useHistory();
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [activePerson, setActivePerson] = useState<Person | null>(null);
  const [modalMode, setModalMode] = useState<Mode>("show");

  const openModal: Function = ({ id, mode }: OpenModalInputs) => {
    const selectedPerson: Person | undefined = people.find(
      person => person.id === id
    );
    if (selectedPerson) {
      setActivePerson(selectedPerson);
    }
    setModalMode(mode);
    setIsModalOpen(true);
  };

  const onHideModal: VoidFunction = () => {
    setIsModalOpen(false);
    setActivePerson(null);
  };
  const handleSubmit: Function = async ({ mode, data }: SubmitFormInputs) => {
    let response = null;
    if (mode === "edit") {
      response = await PeopleAPI.updatePerson({ data });
    }
    if (mode === "create") {
      response = await PeopleAPI.createPerson({ data });
    }
    if (mode === "delete") {
      response = await PeopleAPI.deletePerson({ data });
    }
    if (!response) {
      console.log("no response");
      toast.error("Api error, please check your server connection");
    } else {
      toast.success(`Response ${mode} user: ${JSON.stringify(response)}`);
    }

    setIsModalOpen(false);
    // console.log(, response);
  };

  return (
    <div className="main-table">
      <Button
        className="create-button"
        onClick={() => {
          openModal({ mode: "create" });
        }}
        variant="success"
      >
        {actionsButtons.create.title}
      </Button>
      <Table bordered hover>
        <thead>
          <tr>
            {columns.map(column => (
              <th key={column.key}>{column.name}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {people.map((person, index) => (
            <tr key={person.id}>
              <td>{person.id}</td>
              <td>{person.name}</td>
              <td>{person.age}</td>
              <td>{person.personTypeId === 1 ? "Teacher" : "Student"}</td>
              <td>
                <ButtonGroup>
                  <Button
                    variant="outline-dark"
                    onClick={() => openModal({ id: person.id, mode: "edit" })}
                  >
                    {actionsButtons.edit.title}
                  </Button>
                  <Button
                    variant="danger"
                    onClick={() =>
                      handleSubmit({ mode: "delete", data: person })
                    }
                  >
                    {actionsButtons.delete.title}
                  </Button>
                </ButtonGroup>
              </td>

              <td>
                <ButtonGroup>
                  <Button
                    onClick={() =>
                      history.push({
                        pathname: `/person/${person.id}`,
                        state: {
                          person: person
                        }
                      })
                    }
                  >
                    {actionsButtons.goToChart.title}
                  </Button>
                  <Button
                    onClick={() => openModal({ id: person.id, mode: "show" })}
                    variant="info"
                    disabled={person.personTypeId !== 2}
                  >
                    {actionsButtons.showMore.title}
                  </Button>
                </ButtonGroup>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      {isModalOpen && (
        <Modal
          isOpen={isModalOpen}
          mode={modalMode}
          onHideModal={onHideModal}
          person={activePerson}
          onSubmit={handleSubmit}
        />
      )}
    </div>
  );
};

export default TableComponent;
