import { Person } from "../../types/person.types";
import { Mode } from "../../types/modal.types";

export type TableProps = {
  people: Person[];
};

export type Column = {
  key: number;
  name: string;
};

export type ActionButtonType = {
  title: string;
  variant: string;
};
export type ActionButtonGroupType = {
  edit: ActionButtonType;
  delete: ActionButtonType;
  showMore: ActionButtonType;
  create: ActionButtonType;
  goToChart: ActionButtonType;
};

export type OpenModalInputs = {
  id?: number;
  mode: Mode;
};

export type SubmitFormInputs = {
  mode: "create" | "show" | "edit" | "delete";
  data: Person;
};
