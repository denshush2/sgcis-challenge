import { Column, ActionButtonGroupType } from "./Table.types";
export const columns: Column[] = [
  {
    key: 1,
    name: "Id"
  },
  {
    key: 2,
    name: "Name"
  },
  {
    key: 3,
    name: "Age"
  },
  {
    key: 4,
    name: "Type"
  },
  {
    key: 5,
    name: "Actions"
  },

  {
    key: 6,
    name: "Modal"
  }
];

export const actionsButtons: ActionButtonGroupType = {
  edit: {
    title: "Edit",
    variant: "info"
  },
  delete: {
    title: "Delete",
    variant: "danger"
  },
  showMore: {
    title: "Show more",
    variant: "info"
  },
  create: {
    title: "Create User",
    variant: "success"
  },
  goToChart: {
    title: "Go to chart =>",
    variant: "primary"
  }
};
