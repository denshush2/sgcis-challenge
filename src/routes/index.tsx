import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import MainPage from "../pages/MainPage";
import ChartPage from "../pages/ChartPage";

const Routes: React.FC = () => (
  <Router>
    <Switch>
      <Route path="/" exact component={MainPage} />
      <Route path="/person/:id" children={<ChartPage />} />
    </Switch>
  </Router>
);

export default Routes;
