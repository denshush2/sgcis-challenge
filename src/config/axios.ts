import * as axios from "axios";

const instance = axios.default;

export default instance;
